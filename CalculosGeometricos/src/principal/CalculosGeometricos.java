package principal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CalculosGeometricos {
	

	public static void main(String[] args) {
		try {
			int figura;
			int seleccion;
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("Elige figura una figura por el numero");
			System.out.println("=====================================");
			System.out.println("1.Triangulo; 2.Cuadrado; 3.Rectangulo; 4.Rombo; 5.Romboide; 6.Trapecio");
			figura = Integer.parseInt(br.readLine());
				switch (figura) {
				
				case 1:
					System.out.println("Triangulo:");
					System.out.println("1.Area; 2.Perimetro; 0.Salir");
					seleccion = Integer.parseInt(br.readLine());
					
					while(seleccion != 0) {
						
						if(seleccion == 1) {
							double area;
							double base;
							double altura;
							System.out.println("Valor de la base");
							base = Double.parseDouble(br.readLine());
							System.out.println("Valor de la altura");
							altura = Double.parseDouble(br.readLine());
							area = (base*altura)/2;
							System.out.println("El varo del area es: "+area);
		
						}else if(seleccion == 2) {
							double lado1;
							double lado2;
							double lado3;
							double perimetro;
							System.out.println("Valor del lado 1");
							lado1 = Double.parseDouble(br.readLine());
							System.out.println("Valor del lado 2");
							lado2 = Double.parseDouble(br.readLine());
							System.out.println("Valor del lado 3");
							lado3 = Double.parseDouble(br.readLine());
							perimetro = lado1 + lado2 + lado3;
							System.out.println("El valor del perimetro es: "+perimetro);
						}else {
							System.err.println("Error en la selección");
						}
						System.out.println();
						System.out.println("¿Desea contuniar?");
						System.out.println("==================");
						System.out.println("1.Nueva Area; 2.Nuevo Perimetro; 0.Salir");
						seleccion = Integer.parseInt(br.readLine());
					}
					System.out.println("Fin del programa");
					break;
					
				case 2:
					System.out.println("Cuadrado:");
					System.out.println("1.Area; 2.Perimetro; 0.Salir");
					seleccion = Integer.parseInt(br.readLine());
					
					while(seleccion != 0) {
						
						if(seleccion == 1) {
							double area;
							double lado;
							System.out.println("Valor del lado");
							lado = Double.parseDouble(br.readLine());
							area = Math.pow(lado, 2);
							System.out.println("El varo del area es: "+area);
		
						}else if(seleccion == 2) {
							double lado;
							double perimetro;
							System.out.println("Valor del lado");
							lado = Double.parseDouble(br.readLine());
							perimetro = 4 * lado;
							System.out.println("El valor del perimetro es: "+perimetro);
						}else {
							System.err.println("Error en la selección");
						}
						System.out.println();
						System.out.println("¿Desea contuniar?");
						System.out.println("==================");
						System.out.println("1.Nueva Area; 2.Nuevo Perimetro; 0.Salir");
						seleccion = Integer.parseInt(br.readLine());
					}
					System.out.println("Fin del programa");
					break;
					
				case 3:
					System.out.println("Rectangulo:");
					System.out.println("1.Area; 2.Perimetro; 0.Salir");
					seleccion = Integer.parseInt(br.readLine());
					
					while(seleccion != 0) {
						
						if(seleccion == 1) {
							double area;
							double base;
							double altura;
							System.out.println("Valor de la base");
							base = Double.parseDouble(br.readLine());
							System.out.println("Valor de la altura");
							altura = Double.parseDouble(br.readLine());
							area = base * altura;
							System.out.println("El varo del area es: "+area);
		
						}else if(seleccion == 2) {
							double perimetro;
							double base;
							double altura;
							System.out.println("Valor de la base");
							base = Double.parseDouble(br.readLine());
							System.out.println("Valor de la altura");
							altura = Double.parseDouble(br.readLine());
							perimetro = (2 * base) + (2 * altura);
							System.out.println("El valor del perimetro es: "+perimetro);
						}else {
							System.err.println("Error en la selección");
						}
						System.out.println();
						System.out.println("¿Desea contuniar?");
						System.out.println("==================");
						System.out.println("1.Nueva Area; 2.Nuevo Perimetro; 0.Salir");
						seleccion = Integer.parseInt(br.readLine());
					}
					System.out.println("Fin del programa");
					break;
					
				case 4:
					System.out.println("Rombo:");
					System.out.println("1.Area; 2.Perimetro; 0.Salir");
					seleccion = Integer.parseInt(br.readLine());
					
					while(seleccion != 0) {
						
						if(seleccion == 1) {
							double area;
							double digMayor;
							double digMenor;
							System.out.println("Valor de la diagonal menor");
							digMenor = Double.parseDouble(br.readLine());
							System.out.println("Valor de la diagonal mayor");
							digMayor = Double.parseDouble(br.readLine());
							area = (digMayor * digMenor) / 2;
							System.out.println("El varo del area es: "+area);
		
						}else if(seleccion == 2) {
							double lado;
							double perimetro;
							System.out.println("Valor del lado");
							lado = Double.parseDouble(br.readLine());
							perimetro = lado * 4;
							System.out.println("El valor del perimetro es: "+perimetro);
						}else {
							System.err.println("Error en la selección");
						}
						System.out.println();
						System.out.println("¿Desea contuniar?");
						System.out.println("==================");
						System.out.println("1.Nueva Area; 2.Nuevo Perimetro; 0.Salir");
						seleccion = Integer.parseInt(br.readLine());
					}
					System.out.println("Fin del programa");
					break;
					
				case 5:
					System.out.println("Romboide:");
					System.out.println("1.Area; 2.Perimetro; 0.Salir");
					seleccion = Integer.parseInt(br.readLine());
					
					while(seleccion != 0) {
						
						if(seleccion == 1) {
							double area;
							double base;
							double altura;
							System.out.println("Valor de la base");
							base = Double.parseDouble(br.readLine());
							System.out.println("Valor de la altura");
							altura = Double.parseDouble(br.readLine());
							area = base * altura;
							System.out.println("El varo del area es: "+area);
		
						}else if(seleccion == 2) {
							double perimetro;
							double base;
							double altura;
							System.out.println("Valor de la base");
							base = Double.parseDouble(br.readLine());
							System.out.println("Valor de la altura");
							altura = Double.parseDouble(br.readLine());
							perimetro = (2 * base) + (2 * altura);
							System.out.println("El valor del perimetro es: "+perimetro);
						}else {
							System.err.println("Error en la selección");
						}
						System.out.println();
						System.out.println("¿Desea contuniar?");
						System.out.println("==================");
						System.out.println("1.Nueva Area; 2.Nuevo Perimetro; 0.Salir");
						seleccion = Integer.parseInt(br.readLine());
					}
					System.out.println("Fin del programa");
					break;
					
				case 6:
					System.out.println("Trapecio:");
					System.out.println("1.Area; 2.Perimetro; 0.Salir");
					seleccion = Integer.parseInt(br.readLine());
					
					while(seleccion != 0) {
						
						if(seleccion == 1) {
							double area;
							double baseMenor;
							double baseMayor;
							double altura;
							System.out.println("Valor de la base mayor");
							baseMayor = Double.parseDouble(br.readLine());
							System.out.println("Valor de la base menor");
							baseMenor = Double.parseDouble(br.readLine());
							System.out.println("Valor de la altura");
							altura = Double.parseDouble(br.readLine());
							area = altura * (baseMayor + baseMenor)/2;
							System.out.println("El varo del area es: "+area);
		
						}else if(seleccion == 2) {
							double perimetro;
							double lado1;
							double lado2;
							double lado3;
							double lado4;
							System.out.println("Valor del lado 1");
							lado1 = Double.parseDouble(br.readLine());
							System.out.println("Valor del lado 2");
							lado2 = Double.parseDouble(br.readLine());
							System.out.println("Valor del lado 3");
							lado3 = Double.parseDouble(br.readLine());
							System.out.println("Valor del lado 4");
							lado4 = Double.parseDouble(br.readLine());
							perimetro = lado1 + lado2 + lado3 + lado4;
							System.out.println("El valor del perimetro es: "+perimetro);
						}else {
							System.err.println("Error en la selección");
						}
						System.out.println();
						System.out.println("¿Desea contuniar?");
						System.out.println("==================");
						System.out.println("1.Nueva Area; 2.Nuevo Perimetro; 0.Salir");
						seleccion = Integer.parseInt(br.readLine());
					}
					System.out.println("Fin del programa");
					break;
	
				default:
					System.out.println("Figura no contemplada");
					break;
				}

		}catch(IOException e) {
			System.err.println(e);
		}		
	}
}
